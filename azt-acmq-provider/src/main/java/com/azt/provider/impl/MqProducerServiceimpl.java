package com.azt.provider.impl;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.azt.api.pojo.BaseParam;
import com.azt.api.pojo.MailParam;
import com.azt.api.pojo.SmsParam;
import com.azt.api.service.MqProducerService;

@Service
public class MqProducerServiceimpl implements MqProducerService {

	private static Logger log = LoggerFactory.getLogger(MqProducerServiceimpl.class);
	
	@Autowired
	private JmsTemplate activeMqJmsTemplate;

	@Override
	public void sendMailMessage(MailParam mailParam) {
		sendmessage(mailParam);
	}

	@Override
	public void sendSMSMessage(SmsParam smsParam) {
		sendmessage(smsParam);
	}

	private void sendmessage(final BaseParam param) {
		try {
			
			activeMqJmsTemplate.send(new MessageCreator() {
				public Message createMessage(Session session) throws JMSException {
					String message = JSONObject.toJSONString(param);
					log.info("send param:{}",message);
					return session.createTextMessage(message);
				}
			});
			
			
		} catch (JmsException e) {
			log.error("send param:{}",JSONObject.toJSONString(param));
			log.error("send message product", e);
		}
		
		
	}

}
