package com.azt.api.pojo;

import com.azt.api.enums.MessageEnum;

/**
 * 短信参数
 * @author 张栋  2016年5月17日上午11:44:23
 */
public class SmsParam implements BaseParam {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7988095137250132231L;


	public static final Integer QJT_SMS_SERVICE = 1;//乾璟通短信服务

	public static final Integer LSM_SMS_SERVICE = 2;//螺丝帽短信服务

	private int type = MessageEnum.SMS.getIndex();
	
	//发送的对象手机
	private String to;
	
	private String content;

	private Integer serviceType = QJT_SMS_SERVICE;//服务商类型   1：乾璟通（默认） 2 ：螺丝帽
	
	
	
	public SmsParam(){
		
	}
	public SmsParam(String to,String content){
		this.to = to;
		this.content = content;
	}
	public SmsParam(String to,String content, Integer serviceType){
		this.to = to;
		this.content = content;
		if(serviceType != null){
			this.serviceType = serviceType;
		}
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getServiceType() {
		return serviceType;
	}

	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}
}
