package com.azt.api.service;

import com.azt.api.pojo.MailParam;
import com.azt.api.pojo.SmsParam;

public interface MqProducerService {
	/**
	 * 发送邮件
	 * @param mailParam
	 * @author 张栋  2016年5月17日上午10:25:18
	 */
	public void sendMailMessage(MailParam mailParam);
	
	/**
	 * 发送短息
	 * @param smsParam
	 * @author 张栋  2016年5月17日下午1:56:53
	 */
	public void sendSMSMessage(SmsParam smsParam);
	
	

}
