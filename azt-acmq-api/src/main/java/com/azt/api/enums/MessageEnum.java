package com.azt.api.enums;

/**
 * mq消息
 * @author 张栋  2016年5月17日上午10:28:02
 */
public enum MessageEnum {

	EMAIL(0, "邮箱"), SMS(1, "手机");

	private Integer index;
	private String title;

	private MessageEnum(Integer index, String title) {
		this.index = index;
		this.title = title;
	}

	public static MessageEnum  getEnumByIndex(int index){
		MessageEnum[] values = MessageEnum.values();
		for (int i = 0; i < values.length; i++) {
			MessageEnum messageEnum = values[i];
			if(messageEnum.getIndex() == index){
				return messageEnum;
			}
		}
		return null;
	}
	
	
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
