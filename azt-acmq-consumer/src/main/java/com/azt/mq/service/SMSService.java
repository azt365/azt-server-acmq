package com.azt.mq.service;

import com.azt.api.pojo.SmsParam;
import com.bcloud.msg.http.HttpSender;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;

/**
 * 发送短息
 * 
 * @author 张栋 2016年5月17日上午10:47:02
 */
@Component
public class SMSService {
	
	
	private static Logger  logger = LoggerFactory.getLogger(SMSService.class);
	
	@Value("${sms.secret}")
	private  String key; 
	
	@Value("${sms.sign}")
	private String sign;

	@Value("${sms.QJTSmsUrl}")
	private String QJTSmsUrl;

	@Value("${sms.QJTSmsAccount}")
	private String QJTSmsAccount;

	@Value("${sms.QTJSmsPws}")
	private String QTJSmsPws;
	
	@Autowired
	private ThreadPoolTaskExecutor threadPool;
	
	public  void smsSend(final SmsParam param) {
		threadPool.execute(new Runnable() {
			public void run() {
				try {
					if(param.getServiceType() != null){
						if(param.getServiceType().compareTo(SmsParam.QJT_SMS_SERVICE) == 0){
							if(StringUtils.isNotBlank(param.getTo())){
								try {
									String batchSend = HttpSender.batchSend(QJTSmsUrl, QJTSmsAccount, QTJSmsPws, param.getTo(), param.getContent(), true, null, null);
									logger.info(batchSend);
								}catch (Exception e){
									logger.error("短信错误",e);
									e.printStackTrace();
								}
							}
						}
						if(param.getServiceType().compareTo(SmsParam.LSM_SMS_SERVICE) == 0){
							String to = param.getTo();
							if(StringUtils.isNotBlank(to)){
								Client client = Client.create();
								client.addFilter(new HTTPBasicAuthFilter("api", key));
								WebResource webResource;
								MultivaluedMapImpl formData = new MultivaluedMapImpl();

								if(to.indexOf(",")!=-1 ){
									logger.info("批量发送:{}",to);
									webResource = client.resource("http://sms-api.luosimao.com/v1/send_batch.json");
									formData.add("mobile_list", param.getTo());
								}else{
									logger.info("单条发送:{}",to);
									webResource = client.resource("http://sms-api.luosimao.com/v1/send.json");
									formData.add("mobile", param.getTo());
								}

								formData.add("message",param.getContent()+sign);
								ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
								String textEntity = response.getEntity(String.class);
								int status = response.getStatus();
								logger.info(textEntity);
							}
						}
					}
					
				} catch (MailException e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}
